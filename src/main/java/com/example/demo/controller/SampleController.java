package com.example.demo.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.dto.SampleDTO;

@Controller
@RequestMapping("/sample")
public class SampleController {

	@GetMapping("/ex1")
	public void ex1() {
		
	}
	@GetMapping("/ex2")
	public void ex2(Model model) { // Model은 화면에 값을 전달하는 역할                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
		model.addAttribute("msg","안녕하세요"); // (변수명 , 값) 화면에 문자열 전달
	}
	
	@GetMapping("/ex3")
	public void ex3(Model model , String param) {
		model.addAttribute("aaa",param);
	}
	
	@GetMapping("ex4")
	public void ex4(Model model) {
		model.addAttribute("msg","3번째페이지입니다");
	}
	
	@GetMapping("/ex5")
	public void ex5(Model model) {
		SampleDTO sampleDTO = new SampleDTO(1,"aaa", LocalDateTime.now());
		model.addAttribute("dto",sampleDTO);
		
		List<SampleDTO> list = new ArrayList<>();
		list.add(new SampleDTO(1,"aaa", LocalDateTime.now()));
		list.add(new SampleDTO(2,"bbb", LocalDateTime.now()));
		list.add(new SampleDTO(3,"ccc", LocalDateTime.now()));
		model.addAttribute("list",list);
	}
	
	@GetMapping("/ex6")
	public void ex6(Model model) {
		List<SampleDTO> list = new ArrayList<>();
		list.add(new SampleDTO(1,"aaa", LocalDateTime.now()));
		list.add(new SampleDTO(2,"bbb", LocalDateTime.now()));
		list.add(new SampleDTO(3,"ccc", LocalDateTime.now()));
		model.addAttribute("list",list);
	}
	@GetMapping("/ex7")
	public void ex7(Model model) {
		SampleDTO samplDTO = new SampleDTO(1,"aaa", LocalDateTime.now());
		model.addAttribute("result", "success");
		model.addAttribute("dto",samplDTO);
	}
	
	@GetMapping("/ex8")
	public void ex8(Model model) {
		model.addAttribute("date", LocalDateTime.now());
	}
}